import { Component } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent {      
  

  menuItems = [
    { 
      label:"Inicio",
      icon: "pi pi-home",
      routerLink: 'inicio',
      routerLinkActive: "active",
     },

    {
      label: "Datos básicos",
      icon: 'pi  pi-user-edit',
      className: "menubar-root",
      routerLink: "datos-basicos",
      routerLinkActive: "active",
    },
    {
      label: "Producción bibliográfica",
      icon: 'pi pi-book', 
      items: [
        {label: "Artículos",
       routerLink: "articulos",
       routerLinkActive: "active",}
      ]
    },
    {
      label: "Producción técnica y tecnológica",
      icon: 'pi pi-chart-line',
      routerLinkActive: "active",
    },
    {
      label: "Visualizar currículo",
      icon: 'pi pi-id-card',
      routerLinkActive: "active",
    },            

  ];


  constructor(){

  }

}
