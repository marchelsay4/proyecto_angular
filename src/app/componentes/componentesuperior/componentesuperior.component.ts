import { Component } from '@angular/core';

@Component({
  selector: 'app-componentesuperior',
  templateUrl: './componentesuperior.component.html',
  styleUrls: ['./componentesuperior.component.css']
})
export class ComponentesuperiorComponent {
  constructor (){
    console.log("Hola, soy el componente superior, estoy en el constructor y me ejecuto cada que se muestre la página")
  }
}
