import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './paginas/paginaprincipal/inicio.component';
import { DatosBasicosComponent } from './paginas/datosBasicos/datosbasicos.component';
import { BienvenidaComponent } from './componentes/bienvenida/bienvenida.component';
import { ModuloarticulosComponent } from './paginas/articulos/moduloarticulos/moduloarticulos.component';
import { FormarticuloComponent } from './paginas/articulos/formarticulo/formarticulo.component';
import { LogginComponent } from './paginas/login/loggin.component';
import { PaginanoencontradaComponent } from './paginas/paginaNoEncontrada/paginaNoEncontrada.component';

const routes: Routes = [
  {
    path:'ingreso',
    component: LogginComponent
  },
  
  {
    path:'',
    component: InicioComponent,
    children:[
      {
        path: 'inicio',
        component: BienvenidaComponent

      },
      {
        path:'articulos',
        component: ModuloarticulosComponent
      },
      {
        path:'articulos/crear-articulo',
        component: FormarticuloComponent
      },

      {
        path:'datos-basicos',
        component: DatosBasicosComponent
      },
      {
        path: '',
        redirectTo: '/ingreso',
        pathMatch: 'full' 
      }

    ]
  },
  {
    path: '**',
    component: PaginanoencontradaComponent 
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
