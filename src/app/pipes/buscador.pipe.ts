import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'buscador'
})
export class BuscadorPipe implements PipeTransform {

  transform(value: any, args: any): any {
    
      const resultado = [];
      for (const registro of value){
        if (registro.title.indexOf() > -1){
          resultado.push(registro)
        };
      };
      return resultado;
    
  }

}
