import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalizar'
})
export class CapitalizarPipe implements PipeTransform {

  transform(value: string, todas: boolean= true): any {
    value = value.toLocaleLowerCase();
    let palabras = value.split('');

    if (todas){
      palabras = palabras.map(palabra =>{
        return palabra[0].toUpperCase() + palabra.substring(1); 
      })
    }else{
       palabras [0] = palabras[0][0].toUpperCase() + palabras[0].substring(1);
    }
    return palabras.join(' ');

  }
  
}
