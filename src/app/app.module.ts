import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { registerLocaleData } from '@angular/common';
import localEs from '@angular/common/locales/es';

registerLocaleData(localEs);

import {HttpClientModule }  from '@angular/common/http';


//COMPONENTES DEL PROYECTO
import { BienvenidaComponent } from './componentes/bienvenida/bienvenida.component';
import { CargandoComponent } from './componentes/cargando/cargando.component';
import { ComponentesuperiorComponent } from './componentes/componentesuperior/componentesuperior.component';
import { ComponenteinferiorComponent } from './componentes/componenteinferior/componenteinferior.component';
import { InformacioninicioComponent } from './componentes/informacioninicio/informacioninicio.component';
import { MenuComponent } from './componentes/menu/menu.component';

//PÁGINAS DEL PROYECTO
import { LogginComponent } from './paginas/login/loggin.component';
import { InicioComponent } from './paginas/paginaprincipal/inicio.component';
import { DatosBasicosComponent } from './paginas/datosBasicos/datosbasicos.component';
import { ModuloarticulosComponent } from './paginas/articulos/moduloarticulos/moduloarticulos.component';
import {FormarticuloComponent} from './paginas/articulos/formarticulo/formarticulo.component';
import { EdicionArticuloComponent } from './paginas/articulos/edicionArticulo/edicionArticulo.component';
import { DetalleArticuloComponent } from './paginas/articulos/detalleArticulo/detalleArticulo.component';


import { PaginanoencontradaComponent } from './paginas/paginaNoEncontrada/paginaNoEncontrada.component';

//COMPONENTES DE PRIMENG
import { BadgeModule } from 'primeng/badge';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { CalendarModule } from 'primeng/calendar';
import { ChipModule } from 'primeng/chip';
import { DividerModule } from 'primeng/divider';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { FieldsetModule } from 'primeng/fieldset';
import { InputMaskModule } from 'primeng/inputmask';
import { InputNumberModule } from 'primeng/inputnumber';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { PanelMenuModule } from 'primeng/panelmenu';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { TooltipModule } from 'primeng/tooltip';
import { SplitterModule } from 'primeng/splitter';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EliminacionArticuloComponent } from './paginas/articulos/eliminacionArticulo/eliminacionArticulo.component';
import { MessageService } from 'primeng/api';


//SERVICIOS

import { ArticuloService } from './services/articulo.service';
import { BuscadorPipe } from './pipes/buscador.pipe';
import { CapitalizarPipe } from './pipes/capitalizar.pipe';




@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    ComponentesuperiorComponent,
    DatosBasicosComponent,
    InformacioninicioComponent,
    MenuComponent,
    FormarticuloComponent,
    ComponenteinferiorComponent,
    BienvenidaComponent,
    ModuloarticulosComponent,
    LogginComponent,
    EdicionArticuloComponent,
    PaginanoencontradaComponent,
    EliminacionArticuloComponent,
    DetalleArticuloComponent,
    BuscadorPipe,
    CargandoComponent,
    CapitalizarPipe

  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    ButtonModule,
    BrowserAnimationsModule,
    BadgeModule,
    CalendarModule,
    CheckboxModule,
    ChipModule,
    DialogModule,
    DividerModule,
    DropdownModule,
    FieldsetModule,
    HttpClientModule,
    InputMaskModule,
    InputNumberModule,
    InputTextModule,
    InputTextareaModule,
    PanelMenuModule,
    ProgressSpinnerModule,
    RadioButtonModule,
    SplitterModule,
    TableModule,
    ToastModule,
    TooltipModule,
    ReactiveFormsModule,
    FormsModule,
    

    
  ],

  //Me estaba sacando un error y agregué el messageservise en providers
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'es'
    },
    
    MessageService, ArticuloService],
  bootstrap: [AppComponent]
})
export class AppModule { }
