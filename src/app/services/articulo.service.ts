import { Injectable } from '@angular/core';
import { Articulo  } from '../paginas/articulos/moduloarticulos/articulo';
//import DatosArticulos from '/assets/json/datosarticulos.json';
import DatosArticulos from '../paginas/articulos/moduloarticulos/datosarticulos.json';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { map, take } from 'rxjs/operators';


 @Injectable({
  providedIn: 'root'
})
export class ArticuloService {
   
  private articulos: Articulo[];
  private articulos$: Subject<Articulo[]>;

  constructor(private http: HttpClient) { 
    console.log("Servicio ejecutándose");
    this.articulos = []
    this.articulos$ = new Subject();
  }


  //MÉTODO PARA OBTENER LA LISTA DE ARTÍCULOS:
  getArticulos(){
    return this.articulos.slice();
  }

  //MÉTODO PARA OBTENER UN ARTÍCULO
  getArticulo(index:number){
    return this.articulos[index-1];
  }

  
  
  //1. METODO TOMADOS DE TUTORIAL DE API CRUD, PERMITE GUARDAR ARTÍCULO

  postArticulo(data: any){
    return this.http.post<any>("http://localhost:3000/listaArticulos/", data);
  }

  //2. MÉTODO TOMADO DE TUTORIAL DE API CRUD, PERMITE OBTENER LA LISTA
  getListaArticulos(){
    return this.http.get<any>("http://localhost:3000/listaArticulos/");
  }

  //3. MÉTODO PARA OBTENER UN SOLO ARTÍCULO
  getArticulo2(id: any){
    return this.http.get<any>('http://localhost:3000/listaArticulos/?id=' + id)
    //.pipe(
     //map((response: any) => response.json())
    
    
  }


  getArticulo3(id:any){
    return this.http.get<any>('http://localhost:3000/listaArticulos/?id=' +id)
  }

}
