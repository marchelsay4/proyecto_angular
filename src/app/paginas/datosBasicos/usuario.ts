export interface Usuario{
    nombrePersona: string;
    apellidosPersona: string;
    tipoDocumento: string;
    numDocumento: string;
    fechaNacimiento: string;
    estadoCivil: string;
}