import { Component } from '@angular/core';
import { FormGroup , FormBuilder, FormControl, Validators } from '@angular/forms';
import { Usuario } from './usuario';
import { MessageService } from 'primeng/api';


@Component({
  selector: 'app-datosbasicos',
  templateUrl: './datosbasicos.component.html',
  styleUrls: ['./datosbasicos.component.css']
})

export class DatosBasicosComponent {

  estadoCivil = [
      {
        label: "Soltero" 
      },
      {
        label: "Casado"
      },
      {
        label: "Unión libre"
      },
      {
        label: "Viudo"
      }
  ]


  tipoDocumento = [
    {
      label: "Cédula de ciudadanía" 
    },
    {
      label: "Tarjeta de identidad"
    },
    {
      label: "Pasaporte"
    },
    {
      label: "Cédula de extranjería"
    }
]



  constructor(private messageService: MessageService, private fb: FormBuilder){
  }

  disabled:boolean=false;


  formDatosBasicos = new FormGroup ({
    'nombrePersona': new FormControl ('', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]), 
    'apellidosPersona': new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]), 
    'tipoDocumento': new FormControl ('', [Validators.required]),
    'numDocumento': new FormControl('', [Validators.required, Validators.minLength(7), Validators.maxLength(12)]), 
    'fechaNacimiento': new FormControl ('', [Validators.required]),
    'estadoCivil': new FormControl ('', [Validators.required]),
  })

  procesar(){
    console.log(this.formDatosBasicos.value)
    let usuario = { } as Usuario;
    usuario.nombrePersona = this.formDatosBasicos.get('nombrePersona')!.value || ' ';
    usuario.apellidosPersona = this.formDatosBasicos.get('apellidosPersona')!.value || ' ';
    usuario.tipoDocumento = this.formDatosBasicos.get('tipoDocumento')!.value || ' ';
    usuario.numDocumento = this.formDatosBasicos.get('numDocumento')!.value || ' ';
    usuario.fechaNacimiento = this.formDatosBasicos.get ('fechaNacimiento')!.value || ' ';
    usuario.estadoCivil = this.formDatosBasicos.get('estadoCivil')!.value || ' ';
    

    this.messageService.add({ severity: 'success', summary: 'Información guardada', detail: 'Artículo almacenado exitosamente' });
  }


}
