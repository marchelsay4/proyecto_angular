import { Component } from '@angular/core';
import { FormGroup , FormBuilder , FormControl , Validators } from '@angular/forms';
import { Router, RouterLink } from '@angular/router';
import { MessageService } from 'primeng/api';
import { timeout } from 'rxjs';

@Component({
  selector: 'app-loggin',
  templateUrl: './loggin.component.html',
  styleUrls: ['./loggin.component.css']
})
export class LogginComponent {


login : FormGroup;

cargando: boolean = false;

//MESAGESERVICE sirve para incluir los mensajes de validación, FormBuilder para construir el formulario, Router, para incluir una ruta o que redireccione
constructor(private messageService: MessageService, 
  private fb: FormBuilder , private router: Router  ) {
  
    this.login = this.fb.group(
     {
      tipoDocumento: ['', Validators.required],
      numDocumento: ['', Validators.required],
      clave: ['', Validators.required],
     }
    )
} 

tipoDocumento = [
  {
    label: "Cédula de ciudadanía" 
  },
  {
    label: "Tarjeta de identidad"
  },
  {
    label: "Pasaporte"
  },
  {
    label: "Cédula de extranjería"
  }
]



ingresar(){
  console.log(this.login);
  const tipoDocumento = this.login.value.tipoDocumento;
  const numDocumento = this.login.value.numDocumento;
  const clave = this.login.value.clave;
  
  //Probando poner varias variables en una sola
  const resumen = `
  ${tipoDocumento}  
  ${numDocumento} 
  ${clave}`; 
  console.log("hola", resumen);

  
  return new Promise ((resolve, reject) =>{

    if(numDocumento == "1015436695" && clave =="marce123"){
      console.log("Muy bien");
      this.cargandoIngreso();
      resolve('Usuario autenticado')
  
    }else{
      console.log("Muy mal")
     this.mostrarErrorLogin();
     //El .reset borra o resetea el formulario, en este caso, cuando no se diligencian los datos correctos
     this.login.reset();
     reject('Datos incorrectos')
    }

  });

}


mostrarErrorLogin() {
  this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Sus datos de ingreso son incorrectos.' });
}

cargandoIngreso(){
  this.cargando=true;
  setTimeout(() => {
    this.router.navigate(['inicio'])
  }, 1500);
}



}
