import { Component, ViewChild, ViewChildren, OnInit, Output, EventEmitter } from '@angular/core';
import { Articulo } from './articulo';
import { EdicionArticuloComponent } from '../edicionArticulo/edicionArticulo.component';
import { EliminacionArticuloComponent } from '../eliminacionArticulo/eliminacionArticulo.component';
import { ArticuloService } from 'src/app/services/articulo.service';
import { DetalleArticuloComponent } from '../detalleArticulo/detalleArticulo.component';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-moduloarticulos',
  templateUrl: './moduloarticulos.component.html',
  styleUrls: ['./moduloarticulos.component.css']
})
export class ModuloarticulosComponent implements OnInit {


  //ViewChild: Permite referenciar un componente. Usar sus métodos y atributos. Busca instanciar clase y emplear sus métodos. Viwchild devuelve el primer componente coincidente y Viewchildren, todos los elementos. 
  @ViewChild(EdicionArticuloComponent) edicionComponent: any;
  //@ViewChildren(EdicionArticuloComponent) edicionComponent2 : any;
  @ViewChild(EliminacionArticuloComponent) eliminacionComponent: any;
  @ViewChild(DetalleArticuloComponent) detalleComponent: any;

  @Output ('buscar') buscarEmitter = new EventEmitter 


  //VARIABLE "articulos" GUARDARÁ EL ARREGLO DE ARTÍCULOS OBTENIDOS DEL SERVICIO (Y ÉSTE DEL JSON)
  articulos: Articulo[] = [];
  idArticulo!: number;
  dialogoDetalleArticulo: boolean = false;
  dialogoEliminacionArticulo: boolean = false;
  dialogoEdicionArticulo: boolean = false;
  buscadorArticulo = ''; 


  constructor(
    private _articuloService: ArticuloService,
    private fb: FormBuilder) {
  }





  //DEL SERVICIO OBTENGO LOS ARTÍCULOS
  ngOnInit() {
    this.listandoArticulos();
    //this.articulos = this._articuloService.getArticulos();
    //console.log("Obteniendo los artículos del servicio", this.articulos)
  }





  listandoArticulos(){
    this._articuloService.getListaArticulos()
    .subscribe({
      next: (res)=>{
        this.articulos= res;
      },
      error: (err)=>{
        alert("Error obteniendo los artículos")
      }
    })
  }

  /*filtrarArticulos(event: Event){
    const filtrarValor = (event.target as HTMLInputElement).value;
    this.articulos.filter = filtrarValor.trim().toLowerCase();
  }*/



  mostrarDialogoDetalleArticulo(id: number) {
    this.dialogoDetalleArticulo = true;
    console.log("Child", this.detalleComponent);
    this.detalleComponent.obtenerArticulo(id);
  }


  mostrarDialogoEdicionArticulo(id: number) {
    this.dialogoEdicionArticulo = true;
    console.log("Child", this.edicionComponent);
    //this.edicionComponent.prueba(id);
    this.idArticulo = id;
    console.log("ID desde el padre", this.idArticulo)
  }

  //Esta es otra opción para comunicar el padre con el hijo y pasarle propiedades del componente padre al hijo
  //this.idArticulo = id;


  mostrarDialogoEliminacionArticulo(id: number) {
    this.dialogoEliminacionArticulo = true;
    console.log("Eliminando", this.eliminacionComponent);
    this.eliminacionComponent.prueba(id);
  }

  cerrarDialogo(e: boolean) {
    this.dialogoEliminacionArticulo = e;
  }

 /*
  limpiar() {
    this.buscadorArticulo.reset()
  }*/



}