export interface Articulo {
    id?:any;
    tipoArticulo: {};
    nombreArticulo: string;
    fecha: Date;
    revista: string;
    issnRevista: string;
    estado: string;
    volumen?: string;
    numero: string;
    resumen: string;
}
