import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Articulo } from '../moduloarticulos/articulo';
import { ArticuloService } from 'src/app/services/articulo.service';


@Component({
  selector: 'app-edicionArticulo',
  templateUrl: './edicionArticulo.component.html',
  styleUrls: ['./edicionArticulo.component.css']
})
export class EdicionArticuloComponent implements OnInit {

  //Probando comunicacion
  @Input() id?: number;

  @Input() visible?: boolean;
  //EventEmitter envía datos al padre
  @Output() cerrarEvent = new EventEmitter();

  max: number = 100;
  articulo: any = {};
  


  constructor(
    private messageService: MessageService,
    private fb: FormBuilder,
    private _articuloService: ArticuloService) {



  }

  //DE
  ngOnInit() {
    console.log("MOSTRANDO ID", this.id);
    let id: number = this.id ? this.id : 0;
    this.prueba(id);

  }


 editArticulo = new FormGroup({
    'tipoArticulo': new FormControl('', Validators.required),
    'nombreArticulo': new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(200)]),
    'fecha': new FormControl('', Validators.required),
    'revista': new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(200)]),
    'issnRevista': new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(9)]),
    'estado': new FormControl(''),
    'volumen': new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
    'numero': new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
    'resumen': new FormControl('', [Validators.required, Validators.minLength(20), Validators.maxLength(100)])

  });



  get tipoArticulo() {
    return this.editArticulo.get('tipoArticulo') as FormControl
  }

  get nombreArticulo() {
    return this.editArticulo.get('nombreArticulo') as FormControl
  }


  get fecha() {
    return this.editArticulo.get('fecha') as FormControl
  }

  get revista() {
    return this.editArticulo.get('revista') as FormControl
  }


  get issnRevista() {
    return this.editArticulo.get('issnRevista') as FormControl

  }

  get estado() {
    return this.editArticulo.get('estado') as FormControl
  }


  get volumen() {
    return this.editArticulo.get('volumen') as FormControl
  }

  get numero() {
    return this.editArticulo.get('numero') as FormControl
  }

  get resumen() {
    return this.editArticulo?.get('resumen') as FormControl
  }



  //GENERALMENTE EN EL VALUE VA A UN ID O UN IDENTIFICADOR:
  items = [
    {
      label: "Completo",
      value: "Completo",
    },
    {
      label: "Corto",
      value: "Corto",

    },
    {
      label: "Revisión",
      value: "Revisión",
    },
    {
      label: "Caso Clínico",
      value: "Caso Clínico",
    }
  ]


  editar() {

    console.log(this.editArticulo.value)
    console.log(this.editArticulo.get('volumen')?.value)
    let articulo = {} as Articulo;
    articulo.tipoArticulo = this.editArticulo.get('tipoArticulo')!.value || ' ';
    articulo.nombreArticulo = this.editArticulo.get('nombreArticulo')!.value || ' ';
    articulo.fecha = new Date(this.editArticulo.get('fecha')!.value!);
    articulo.revista = this.editArticulo.get('revista')!.value || ' ';
    articulo.issnRevista = this.editArticulo.get('issnRevista')!.value || ' ';
    articulo.volumen = this.editArticulo.get('volumen')!.value || ' ';
    articulo.numero = this.editArticulo.get('numero')!.value || ' ';
    articulo.resumen = this.editArticulo.get('resumen')!.value || ' ';

    this.messageService.add({ severity: 'success', summary: 'Información guardada', detail: 'Artículo editado exitosamente' });
  }




  //EL MENSAJE DE GUARDADO LO ESTOY MOSTRANDO EN EDITAR
  /*showSuccess() {
    this.messageService.add({ severity: 'success', summary: 'Información guardada', detail: 'Artículo almacenado exitosamente' });
  }*/

  //longResumen = this.resumen.errors?.['maxlength']?.requiredLength - this.resumen.errors?.['minlength']?.actualLength; 


  public prueba(id: number) {
    console.log("Edición Artículo", id);
    this.articulo = this._articuloService.getArticulo(id);
    console.log("Obteniendo un artículo del servicio", this.articulo);



    this.editArticulo = new FormGroup({
      'tipoArticulo': new FormControl(this.articulo.tipoArticulo, Validators.required),
      'nombreArticulo': new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(200)]),
      'fecha': new FormControl('', Validators.required),
      'revista': new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(200)]),
      'issnRevista': new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(9)]),
      'estado': new FormControl(''),
      'volumen': new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
      'numero': new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
      'resumen': new FormControl('', [Validators.required, Validators.minLength(20), Validators.maxLength(100)])
  
    });
  


  }

}
