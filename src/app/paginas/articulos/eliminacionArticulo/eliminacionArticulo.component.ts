import { Component , EventEmitter, Input , Output } from '@angular/core';
import { ArticuloService } from 'src/app/services/articulo.service';


@Component({
  selector: 'app-eliminacionArticulo',
  templateUrl: './eliminacionArticulo.component.html',
  styleUrls: ['./eliminacionArticulo.component.css']
})
export class EliminacionArticuloComponent {

  //Probando comunicacion
  @Input() id?:number;
  
  @Input() visible?: boolean;
  //EventEmitter envía datos al padre
  @Output() cerrarEvent = new EventEmitter();

  constructor( private _articuloService: ArticuloService ){
  }
 
  
  public prueba(id:number){
    console.log("Eliminación Artículo", id)
  }
  
  cerrar (){
    this.cerrarEvent.emit(Boolean);
  }

}
