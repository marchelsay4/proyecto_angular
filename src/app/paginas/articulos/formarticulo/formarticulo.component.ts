import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Articulo } from '../moduloarticulos/articulo';
import { ArticuloService } from 'src/app/services/articulo.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-formarticulo',
  templateUrl: './formarticulo.component.html',
  styleUrls: ['./formarticulo.component.css'],
  //providers: [MessageService]
})

export class FormarticuloComponent {
  max:number = 100;
  cargando: boolean = false;



  constructor(
    private messageService: MessageService, 
    private fb: FormBuilder,
    private _articuloService: ArticuloService,
    private router: Router) {
  }

  /*public articuloForm: FormGroup | undefined;*/

  //ASÍ ASIGNAMOS LAS VALIDACIONES Y CREAMOS E INSTANCIAMOS LA CLASE DEL FORMCONTROL
  formArticulo = new FormGroup ({
    'tipoArticulo': new FormControl ('', Validators.required),
    'nombreArticulo' : new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(200)]),
    'fecha' : new FormControl ('', Validators.required),
    'revista' : new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(200)]),
    'issnRevista' : new FormControl ('', [Validators.required, Validators.minLength(9), Validators.maxLength(9)]),
    'estado' : new FormControl('Pendiente'),
    'volumen' : new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
    'numero' : new FormControl ('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
    'resumen' : new FormControl ('', [Validators.required, Validators.minLength(20), Validators.maxLength(100)])
  
  });
  
  
  //ASÍ OBTENEMOS EL VALOR DEL FORMARTICULO.GET Y EL CAMPO Y SE LO ASIGNAMOS AL CAMPO SIN EL FORMARTICULO.GET
  //el as formcontrol permite quitar el ? 
  get tipoArticulo (){
    return this.formArticulo.get('tipoArticulo') as FormControl
  }

  get nombreArticulo (){
    return this.formArticulo.get('nombreArticulo') as FormControl
  }


  get fecha () {
    return this.formArticulo.get('fecha') as FormControl
  }

  get revista () {
    return this.formArticulo.get('revista') as FormControl
  }


  get issnRevista () {
    return this.formArticulo.get('issnRevista') as FormControl

  }

  get estado () {
    return this.formArticulo.get('estado') as FormControl
  }


  get volumen () {
    return this.formArticulo.get('volumen') as FormControl
  }

  get numero () {
    return this.formArticulo.get('numero') as FormControl
  }

  get resumen () {
    return this.formArticulo.get('resumen') as FormControl
  } 




//NUEVO

/*formArticulo !: FormGroup;


ngOnInit(): void {
  
  this.formArticulo = this.fb.group({
  
    tipoArticulo: ['', Validators.required],
    nombreArticulo: ['', Validators.required, Validators.minLength(3), Validators.maxLength(200)],
    fecha: ['', Validators.required],
    revista: ['', Validators.required, Validators.minLength(3), Validators.maxLength(200)],
    issnRevista: ['', Validators.required, Validators.minLength(9), Validators.maxLength(9)],
    estado: [''],
    volumen: ['', Validators.required, Validators.minLength(1), Validators.maxLength(100)],
    numero: ['', Validators.required, Validators.minLength(1), Validators.maxLength(100)],
    resumen: ['', Validators.required, Validators.minLength(20), Validators.maxLength(100)]
  
  })
}

*/


  crearArticulo(){
    if(this.formArticulo.valid){
      this._articuloService.postArticulo(this.formArticulo.value)
      .subscribe({
        next:(res)=>{
          //alert("Artículo creado")
          //this.formArticulo.reset();
          this.messageService.add({ severity: 'success', summary: 'Información guardada', detail: 'Artículo almacenado exitosamente' });
          this.cargandoGuardar();
        },
        error:()=>{
          alert("Error agregando el artículo")
        }
      })
    }

  }



  
  //ESTE MÉTODO SE EJECUTA CUANDO GUARDAMOS EL FORMULARIO EN EL CLIC DEL BOTÓN
  //AQUÍ VAMOS A GUARDAR LOS CAMPOS DEL FORMGRUP EN LOS CAMPOS QUE CREÉ EN EL MODELO
  procesar(){
    if(this.formArticulo.valid){
      console.log(this.formArticulo.value)
      let articulo = { } as Articulo;
      articulo.tipoArticulo = this.formArticulo.get('tipoArticulo')!.value || ' ';
      articulo.nombreArticulo = this.formArticulo.get('nombreArticulo')!.value || ' ';
      articulo.fecha = new Date (this.formArticulo.get('fecha')!.value!);
      articulo.revista = this.formArticulo.get('revista')!.value!;
      articulo.issnRevista = this.formArticulo.get ('issnRevista')!.value || ' ';
      articulo.volumen = this.formArticulo.get('volumen')!.value || ' ';
      articulo.numero = this.formArticulo.get('numero')!.value || ' ';
      articulo.resumen = this.formArticulo.get('resumen')!.value || ' ';
  
      this.messageService.add({ severity: 'success', summary: 'Información guardada', detail: 'Artículo almacenado exitosamente' });
      this.cargandoGuardar();
    }
}

   



  items = [
      {
        label: "Completo" 
      },
      {
        label: "Corto"
      },
      {
        label: "Revisión"
      },
      {
        label: "Caso Clínico"
      }
  ]

  dialogoRevista: boolean= false;


  mostrarDialogoBuscadorRevista(){
    this.dialogoRevista = true;

  }




showSuccess() {
    this.messageService.add({ severity: 'success', summary: 'Información guardada', detail: 'Artículo almacenado exitosamente' });
}



  longResumen: number = this.resumen.errors?.['maxlength']?.requiredLength - this.resumen.errors?.['minlength']?.actualLength; 


   
  cargandoGuardar(){
    this.cargando=true;
    setTimeout(() => {
      this.router.navigate(['articulos'])
    }, 1500);
  }




}

