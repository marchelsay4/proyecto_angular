import { Component , EventEmitter, Input , Output, OnInit } from '@angular/core';
import { Articulo } from '../moduloarticulos/articulo';
import { ArticuloService } from 'src/app/services/articulo.service';



@Component({
  selector: 'app-detallearticulo',
  templateUrl: './detalleArticulo.component.html',
  styleUrls: ['./detalleArticulo.component.css']
})
export class DetalleArticuloComponent  implements OnInit{

    //Esta propiedad va a venir del padre
    @Input() id?:number;

    //EventEmitter envía datos al padre
    @Output() cerrarEvent = new EventEmitter();
    

    articulo: any = {};

    constructor(
      private _articuloService: ArticuloService ) {
    }



    ngOnInit(){
    }
     
  public obtenerArticulo(id:any){
    console.log("Detalle Artículo", id);
    this.articulo= this._articuloService.getArticulo2(id);
    console.log("Obteniendo un artículo del servicio", this.articulo)
    
  }

}
